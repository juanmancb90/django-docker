Simple config to mount django app in Docker

*Features
 
 dev files: dockerfile, docker-compose, env var and db container
 
 prod files: dockerfile, nginx, gunicorn, docker-compose, env var

*Git clone and create your own files for .env.dev and .env.prod from .env.dev.sample

*docker-componse up -d build
